// provider setup
provider "aws" {
  region                  = "${var.aws_region}"
  profile                 = "${var.aws_profile}"
  shared_credentials_file = "${var.aws_credentials_file}"
}

// query for ubuntu ami
data "aws_ami" "base_image" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/ebs/ubuntu-trusty-14.04-amd64-*"]
  }

  owners = ["${var.canonical_id}"]
}

// key pair
resource "aws_key_pair" "ec2_key" {
  key_name   = "${var.ssh_public_key_name}"
  public_key = "${var.ssh_public_key}"
}

// security groups
resource "aws_security_group" "sg" {
  name        = "${var.firstname}-sg"
  description = "${var.firstname}-sg security rule"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Role = "${var.environment}"
  }
}

// ec2
resource "aws_instance" "ec2" {
  ami                    = "${data.aws_ami.base_image.id}"
  instance_type          = "${var.server_size}"
  count                  = "${var.ec2_instance_count}"
  key_name               = "${aws_key_pair.ec2_key.key_name}"
  vpc_security_group_ids = ["${aws_security_group.sg.id}"]

  tags {
    Name = "${var.firstname}-ec2-${count.index}"
    Role = "${var.environment}"
  }
}

// Create a new load balancer
resource "aws_elb" "wordpress" {
  name               = "wordpress-elb"
  availability_zones = ["us-east-1b", "us-east-1c", "us-east-1d"]

  access_logs {
    bucket   = "wordpress29"
    interval = 60
    enabled  = false
  }

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:80"
    interval            = 10
  }

  instances                   = ["${aws_instance.ec2.*.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
  security_groups             = ["${aws_security_group.sg.id}"]

  tags {
    Name = "wordpress-elb"
  }
}

//AWS_DB_SECURITY_GROUP
resource "aws_security_group" "default" {
  name        = "securitygroup-rds"
  description = "Allow all inbound traffic"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

// AWS_DB_INSTANCE 
resource "aws_db_instance" "default" {
  allocated_storage = 5
  engine            = "mysql"

  #  engine_version       = "5.6.17"
  instance_class         = "db.t2.micro"
  name                   = "dbwordpress"
  username               = "dbuser"
  password               = "dbpassword"
  vpc_security_group_ids = ["${aws_security_group.default.id}"]

  #  db_subnet_group_name = "my_database_subnet_group"

  #  parameter_group_name = "default.mysql5.6"
}

output "platform_details" {
  #  value = ["${aws_instance.ec2.0.public_ip}, ${aws_instance.ec2.0.id},${aws_instance.ec2.1.public_ip}, ${aws_instance.ec2.1.id}"]
  value = ["${aws_instance.ec2.*.public_ip}"]
}

output "elb_details" {
  value = ["${aws_elb.wordpress.id}, ${aws_elb.wordpress.dns_name}"]
}

output "RDS_details" {
  value = ["${aws_db_instance.default.address}"]
}
